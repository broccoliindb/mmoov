import express from 'express'
import routes from '../router'
import { pollHome, getIn, getOut, showResult } from '../controllers/pollControllers'

const pollRouter = express.Router()

pollRouter.get(routes.home, pollHome)
pollRouter.get(routes.getIn, getIn)
pollRouter.get(routes.getOut, getOut)
pollRouter.get(routes.showResult, showResult)

export default pollRouter
