import express from 'express'
import routes from '../router'
import { home, logIn, logOut } from '../controllers/globalControllers'

const globalRouter = express.Router()

globalRouter.get(routes.home, home)
globalRouter.get(routes.logIn, logIn)
globalRouter.get(routes.logOut, logOut)

export default globalRouter
