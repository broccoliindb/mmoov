import express from 'express'
import routes from '../router'
import { planHome, editPlan, removePlan, addPlan, showPlan } from '../controllers/planControllers'

const planRouter = express.Router()

planRouter.get(routes.home, planHome)
planRouter.get(routes.editPlan, editPlan)
planRouter.get(routes.removePlan, removePlan)
planRouter.get(routes.addPlan, addPlan)
planRouter.get(routes.showPlan, showPlan)

export default planRouter
