import routes from './router'

export const localsMiddleware = (req, res, next) => {
  res.locals.siteName = 'MMOOV'
  res.locals.routes = routes
  next()
}
