# MMOOV SKILL SERVER

MMOOV가 사용할 웹서버

## MMOOV가 사용하는 REST API의 대한 정의

<!-- prettier-ignore -->
| 명칭 | Action | 목적 | 비고 |
| --- | ---- | --- | --- |
| 일정등록 | /addDailyPlan | 일정을 등록한다. | POST |
| 일정조회 | /searchDailyPlan | 일정을 조회한다. | GET |
| 일정압박시 | /pushYourDailyPlan | 일정을완료하지 못했을 시 메세지 | GET |
| 투표결과 조회 | /getVoteResult | 멤버들간에 투표진행시 | GET |
| 투표항목등록 | /addVoteItems| 투표항목들 등록시 | POST |
