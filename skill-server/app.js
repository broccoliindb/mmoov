// Sample from Kakaotalk Dev S
// const express = require('express')
// const app = express()
// const logger = require('morgan')
// const bodyParser = require('body-parser')

// const apiRouter = express.Router()

// app.use(logger('dev', {}))
// app.use(bodyParser.json())
// app.use('/api', apiRouter)

// apiRouter.post('/sayHello', function (req, res) {
//   const responseBody = {
//     version: '2.0',
//     template: {
//       outputs: [
//         {
//           simpleText: {
//             text: "hello I'm mmoov"
//           }
//         }
//       ]
//     }
//   }

//   res.status(200).send(responseBody)
// })

// apiRouter.post('/showHello', function (req, res) {
//   console.log(req.body)

//   const responseBody = {
//     version: '2.0',
//     template: {
//       outputs: [
//         {
//           simpleImage: {
//             imageUrl: '../images/bot.jpg',
//             altText: "It's me."
//           }
//         }
//       ]
//     }
//   }

//   res.status(200).send(responseBody)
// })

// app.listen(3000, function () {
//   console.log('Example skill server listening...')
// })
// Sample from Kakaotalk Dev S

// const express = require('express')

import express from 'express'
import morgan from 'morgan'
import helmet from 'helmet'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import pollRouter from './routers/pollRouter'
import planRouter from './routers/planRouter'
import globalRouter from './routers/globalRouter'
import routes from './router'
import { home } from './controllers/globalControllers'
import { localsMiddleware } from './middlewares'

const app = express()

app.use(helmet())
app.set('view engine', 'pug')
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('dev'))
app.use(localsMiddleware)

app.use(routes.poll, pollRouter)
app.use(routes.plan, planRouter)
app.use(routes.home, globalRouter)

app.get(routes.home, home)

export default app
