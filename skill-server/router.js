const HOME = '/'
const LOG_IN = '/login'
const LOG_OUT = '/logout'

// PLAN
const PLAN = '/plan'
const EDIT_PLAN = '/edit-plan'
const ADD_PLAN = '/add-plan'
const REMOVE_PLAN = '/remove-plan'
const SHOW_PLAN = '/show-plan'

// POLL
const POLL = '/poll'
const GET_IN = '/get-in'
const GET_OUT = '/get-out'
const SHOW_RESULT = '/show-result'

const routes = {
  home: HOME,
  logIn: LOG_IN,
  logOut: LOG_OUT,
  plan: PLAN,
  editPlan: EDIT_PLAN,
  addPlan: ADD_PLAN,
  removePlan: REMOVE_PLAN,
  showPlan: SHOW_PLAN,
  poll: POLL,
  getIn: GET_IN,
  getOut: GET_OUT,
  showResult: SHOW_RESULT
}

export default routes
