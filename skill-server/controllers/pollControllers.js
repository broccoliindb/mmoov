export const pollHome = (req, res) => res.render('home', { pageTitle: 'pollHome' })

export const getIn = (req, res) => {
  res.send({ pageTitle: 'getIn' })
}

export const getOut = (req, res) => {
  res.send({ pageTitle: 'getOut' })
}

export const showResult = (req, res) => {
  res.send({ pageTitle: 'showResult' })
}
