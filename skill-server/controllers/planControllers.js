export const planHome = (req, res) => res.render('planHome', { pageTitle: 'planHome' })

export const editPlan = (req, res) => {
  res.send({ pageTitle: 'editPlan' })
}

export const removePlan = (req, res) => {
  res.send({ pageTitle: 'removePlan' })
}

export const addPlan = (req, res) => {
  res.send({ pageTitle: 'addPlan' })
}

export const showPlan = (req, res) => {
  res.send({ pageTitle: 'showPlan' })
}
