export const home = (req, res) => res.render('home', {
  pageTitle: 'globalHome'
})

export const logIn = (req, res) => res.send({ pageTitle: 'logIn' })

export const logOut = (req, res) => {
  res.send('logout')
}
